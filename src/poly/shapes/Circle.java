package poly.shapes;

public class Circle implements Shape {
    private int radius;

    public Circle(int radius) {
        this.radius = radius;
    }

//    public int getRadius() {
//        return radius;
//    }
    @Override
    public double getArea() {
//        System.out.println(Math.PI * Math.pow(radius, 2));
        return Math.PI * Math.pow(radius, 2);
    }

}
