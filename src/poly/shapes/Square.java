package poly.shapes;

public class Square implements Shape {
    private int side;

    public Square(int side) {
        this.side = side;
    }

//    public int getSide() {
//        return side;
//    }
    @Override
    public double getArea() {
//        System.out.println(Math.pow(side, 2));
        return Math.pow(side, 2);
    }
}
