package poly.customer;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class CustomerRepository {

    private static final String FILE_PATH = "src/poly/customer/data.txt";

    private List<String> readAll() {
        try {
            return Files.readAllLines(Paths.get(FILE_PATH));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private void writeToFile(){
        try {
            Files.writeString(Paths.get(FILE_PATH), customers.stream().map(AbstractCustomer::toString).collect(Collectors.joining("\n")));
        } catch (IOException e) {
            throw new RuntimeException("Error " + e);
        }
    }

    private DateTimeFormatter formatter = DateTimeFormatter
            .ofPattern("yyyy-MM-dd");

    public List<AbstractCustomer> getCustomers() {
        try {
            return readAll().stream()
                    .map(line -> {
                        String[] parts = line.split(";");
                        if (parts[0].equals("REGULAR")) {
                            return new RegularCustomer(parts[1],
                                    parts[2],
                                    Integer.parseInt(parts[3]),
                                    LocalDate.parse(parts[4], formatter));
                        } else if (parts[0].equals("GOLD")) {
                            return new GoldCustomer(parts[1],
                                    parts[2],
                                    Integer.parseInt(parts[3])
                                    , LocalDate.now());
                        } else{
                            throw new RuntimeException("Unexpected type of customer");
                        }
                    }).collect(Collectors.toCollection(ArrayList::new));
        } catch (Exception e) {
            throw new RuntimeException("Something wrong with file " + e);
        }
    }

    List<AbstractCustomer> customers = getCustomers();

    public Optional<AbstractCustomer> getCustomerById(String id) {

        for (AbstractCustomer customer : customers) {
            if (customer.getId().equals(id)) {
                return Optional.of(customer);
            }
        }
        return Optional.empty();
    }

    public void remove(String id) {
        Optional<AbstractCustomer> customer = getCustomerById(id);

        customer.ifPresent(abstractCustomer -> customers.remove(abstractCustomer));
    }


    public void save(AbstractCustomer customer) {
        remove(customer.id);
        customers.add(customer);
        writeToFile();
    }

    public int getCustomerCount() {
        return customers.size();
    }
}
