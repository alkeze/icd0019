package poly.customer;

import java.time.LocalDate;
import java.util.Objects;

public class RegularCustomer extends AbstractCustomer {

    private LocalDate date;

    public RegularCustomer(String id, String name,
                           int bonusPoints, LocalDate lastOrderDate) {

        super(id, name, bonusPoints);
        this.date = lastOrderDate;
    }

    @Override
    public void collectBonusPointsFrom(Order order) {
        double total = order.getTotal();

        if (total >= 100) {
            if (order.getDate().getMonthValue() - date.getMonthValue() < 1) {
                total *= 1.5;
            }
        } else {
            total = 0.0;
        }

        super.bonusPoints += Double.valueOf(total).intValue();

//        super.bonusPoints += (total >= 100) ? (order.getDate().getMonthValue() - date.getMonthValue() < 1)
//                ? Double.valueOf(total * 1.5).intValue() : Double.valueOf(total).intValue() : Double.valueOf(total).intValue();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null || this.getClass() != obj.getClass()) {
            return false;
        }

        AbstractCustomer other = (AbstractCustomer) obj;

        return Objects.equals(id, other.id) &&
                Objects.equals(name, other.name) &&
                Objects.equals(bonusPoints, other.bonusPoints);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, bonusPoints, date);
    }

    @Override
    public String asString() {
        return String.format("REGULAR;%s;%s;%d;%s;", id, name, bonusPoints, this.date);
    }

}