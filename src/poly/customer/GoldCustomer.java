package poly.customer;

import java.time.LocalDate;
import java.util.Objects;

public class GoldCustomer extends AbstractCustomer {

    public GoldCustomer(String id, String name,
                        int bonusPoints, LocalDate lastOrderDate) {

        super(id, name, bonusPoints);

    }

    @Override
    public void collectBonusPointsFrom(Order order) {
        double total = order.getTotal();
        super.bonusPoints += total >= 100 ? Double.valueOf(total * 1.5).intValue() : Double.valueOf(total).intValue();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null || this.getClass() != obj.getClass()) {
            return false;
        }

        AbstractCustomer other = (AbstractCustomer) obj;

        return Objects.equals(id, other.id) &&
                Objects.equals(name, other.name) &&
                Objects.equals(bonusPoints, other.bonusPoints);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, bonusPoints);
    }

    @Override
    public String asString() {
        return String.format("GOLD;%s;%s;%d;", id, name, bonusPoints);
    }

}