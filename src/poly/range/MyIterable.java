package poly.range;

import java.util.Iterator;
import java.util.Spliterator;
import java.util.function.Consumer;

public class MyIterable implements  Iterable{
    private final int start;
    private final int end;

    public MyIterable(int start, int end) {
        this.start = start;
        this.end = end;
    }

    @Override
    public Iterator<Integer> iterator() {
        return new MyIterator(start, end);
    }
//
//    @Override
//    public void forEach(Consumer action) {
//
//    }
//
//    @Override
//    public Spliterator spliterator() {
//        return null;
//    }
}
