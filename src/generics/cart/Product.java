package generics.cart;

public class Product implements CartItem {
    private String id;
    private Double price;
//    public Integer quantity;

    public Product(String id, Double price) {
        this.id = id;
        this.price = price;
//        this.quantity = 1;
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public Double getPrice() {
        return price;
    }

//    @Override
//    public Integer getQuantity(){
//        return quantity;
//    }
//
//    @Override
//    public void increaseQuantity(){
//        quantity += 1;
//    }

    @Override
    public String toString() {
        return String.format("%s, %.1f", id, price).replaceAll("(?<=\\d),(?=\\d)", ".");
    }
}
