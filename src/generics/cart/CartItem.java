package generics.cart;

public interface CartItem {

    String getId();

    Double getPrice();

//    Integer getQuantity();
//
//    void increaseQuantity();

}
