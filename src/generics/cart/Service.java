package generics.cart;



public class Service implements CartItem {
    private String serviceId;
    private Double salePrice;
//    public Integer quantity;

    public Service(String id, Double price) {
        this.serviceId = id;
        this.salePrice = price;
//        this.quantity = 1;
    }

    @Override
    public String getId() {
        return serviceId;
    }

    @Override
    public Double getPrice() {
        return salePrice;
    }

//    @Override
//    public Integer getQuantity(){
//        return quantity;
//    }
//
//    @Override
//    public void increaseQuantity(){
//        quantity += 1;
//    }

    @Override
    public String toString() {
        return String.format("%s, %.1f", serviceId, salePrice).replaceAll("(?<=\\d),(?=\\d)", ".");
    }
}
