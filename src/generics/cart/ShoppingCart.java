package generics.cart;

import java.util.ArrayList;
import java.util.List;
import java.util.StringJoiner;

public class ShoppingCart<T extends CartItem> {
    private List<T> storage;
    private double discount;
    private double lastDiscount;

    public ShoppingCart() {
        this.storage = new ArrayList<>();
        this.discount = 1D;
    }


    public void add(T item) {
        storage.add(item);
    }

 /*   private int getItemIndexById(T item) {
        System.out.println(item);
        for (int i = 0; i < storage.size(); i++) {
            System.out.println(storage.get(i));
            if (storage.get(i).getId().equals(item.getId())) {
                return i;
            }
        }
        return -1;
    }
*/
    private T getItemById(String id) {
        T returnItem = null;
        if (containsId(id, storage)) {
            for (T t : storage) {
                if (t.getId().equals(id)){
                    returnItem = t;
                }
            }
        }

        return returnItem;
    }

    private boolean containsId(String id, List<T> list) {
        for (T t : list) {
            if (t.getId().equals(id)) {
                return true;
            }
        }
        return false;
    }

    public void removeById(String id) {
        storage.remove(getItemById(id));
    }


    public Double getTotal() {
        double total = 0D;

        for (T item : storage) {
            total += item.getPrice();
        }
        return total * discount;
    }

    public void increaseQuantity(String id) {
        if (containsId(id, storage)) {
            storage.add(getItemById(id));
        }
    }


    public void applyDiscountPercentage(Double discount) {
        lastDiscount = 1 - (discount / 100);
        this.discount *= 1 - (discount / 100);
    }

    public void removeLastDiscount() {
        this.discount /= lastDiscount;
    }

    public void addAll(List<? extends T> items) {
        for (T t : items) {
            add(t);
        }
    }

    private int howManyById(String id){
        int counter = 0;
        for (T t: storage) {
            if(t.getId().equals(id)){
                counter++;
            }
        }
        return counter;
    }

    private String preparingString(){
        List<T> preparingList = new ArrayList<>();
        StringJoiner joiner = new StringJoiner("), ", "", ")");
        for(T t: storage){
            if(!containsId(t.getId(), preparingList)){
                preparingList.add(t);
            }
        }
        for (T t: preparingList){
            joiner.add("(" + t.toString() + ", "+ howManyById(t.getId()));
        }
        return joiner.toString();
    }

    @Override
    public String toString() {
        return preparingString();
//                storage.toString().replace("[", "").replace("]", "");
    }
}
