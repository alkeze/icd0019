package oo.hide;

public class Fibonacci {
    public Integer fibonacci(Integer i){
        if(i == 0){
            return 0;
        }
        if(i == 1){
            return 1;
        }
        return fibonacci(i - 1) + fibonacci(i - 2);
    }
    private Integer v = 0;
    public Integer nextValue() {
        return fibonacci(v++);
    }
}