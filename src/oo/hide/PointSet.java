package oo.hide;

public class PointSet {
    private Point[] set;
    private int freePosition;
    private int capacity;

    public PointSet(int capacity) {
        set = new Point[capacity];
        this.freePosition = 0;
        this.capacity = capacity;
    }

    public PointSet() {
        this(10);
    }

    public void add(Point point) {
        if (!contains(point)) {
            if (freePosition >= set.length) {
                int dubbler;
                if (capacity == 0) {
                    dubbler = set.length + 1;
                } else {
                    dubbler = capacity * 2;
                }
                Point[] newSet = new Point[dubbler];
                System.arraycopy(set, 0, newSet, 0, set.length);
                set = newSet;
            }
            set[freePosition++] = point;
        }
    }

    public int size() {
        int size = 0;
        for (Point point : this.set) {
            if (point != null) {
                size++;
            }
        }
        return size;
    }

    public boolean contains(Point p) {
        for (Point point : set) {
            if (point != null && point.equals(p)) {
                return true;
            }
        }
        return false;
    }

    public Boolean containsAnything() {
        for (Point point : this.set) {
            if (point != null) {
                return true;
            }
        }
        return false;
    }

    public Point[] removeNulls() {
        int noNulls = 0;
        for (Point p : this.set) {
            if (p != null) {
                noNulls++;
            }
        }
        Point[] result = new Point[noNulls];
        int pos = 0;
        for (Point p : this.set) {
            if (p != null) {
                result[pos++] = p;
            }
        }
        return result;
    }

    @Override
    public boolean equals(Object other) {
        if (!(other instanceof PointSet)) {
            return false;
        }
        PointSet p = (PointSet) other;
        if (this.set.length != p.set.length) {
            return false;
        }
        if (!this.containsAnything() && !p.containsAnything()) {
            return true;
        }
        PointSet thisPoint = this;
        PointSet thatPoint = p;
        if (p.containsAnything() && this.containsAnything()) {
            thatPoint.set = thatPoint.removeNulls();
            thisPoint.set = thisPoint.removeNulls();
        }
        for (Point point : thatPoint.set) {
            if (!thisPoint.contains(point)) {
                return false;
            }
        }
        return true;
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        for (Point elem : set) {
            if (elem != null) {
                result.append(", (").append(elem).append(")");
            }
        }
        result = new StringBuilder(result.toString().replaceFirst(", ", ""));
        return result.toString();
    }

    public PointSet subtract(PointSet other) {
        PointSet result = new PointSet();
        for (Point point: this.set) {
            if(!other.contains(point)){
                result.add(point);
            }
        }
        return result;
    }

    public PointSet intersect(PointSet other) {
        PointSet result = new PointSet();
        for (Point point: this.set) {
            if(other.contains(point) && this.contains(point)){
                result.add(point);
            }
        }
        return result;
    }
}
