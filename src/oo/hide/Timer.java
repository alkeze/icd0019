package oo.hide;

public class Timer {
    private long time = System.currentTimeMillis();
    public String getPassedTime() {
        double result = System.currentTimeMillis() - time;
        return String.format("%s seconds ", result / 1000);
    }
}
