package oo.hide;

public class Counter {
    private Integer start;
    private Integer step;
    public Counter(Integer start, Integer step) {
        this.start = start;
        this.step = step;
    }
    private boolean flag = true;
    public Integer nextValue() {
        if(flag){
            flag = false;
            return start;
        }
        return start += step;
    }
}