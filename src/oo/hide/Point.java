package oo.hide;

import java.util.Objects;

public class Point {

    private Integer x;
    private Integer y;

    public Point(Integer x, Integer y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public String toString() {
        return String.format("%s, %s", x, y);
    }

    @Override
    public boolean equals(Object obj) {
        if(!(obj instanceof Point)){
            return false;
        }
        Point b = (Point) obj;
        return Objects.equals(x, b.x) && Objects.equals(y, b.y);
    }
}
