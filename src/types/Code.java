package types;

import java.util.Arrays;

public class Code {

    public static void main(String[] args) {

        int[] numbers = {1, 3, -2, 9, -10, 320};

        System.out.println(sum(numbers)); // 11

        System.out.println(average(numbers));

        System.out.println(minimumElement(numbers));

        System.out.println(asString(numbers));

        System.out.println(squareDigits("a9b2"));
    }

    public static int sum(int[] numbers) {
        int result = 0;
        for (int number : numbers) {
            result += number;
        }
        return result;
    }

    public static double average(int[] numbers) {
        return Double.valueOf(sum(numbers)) / numbers.length;
    }

    public static Integer minimumElement(int[] integers) {
        if (integers.length == 0) {
            return null;
        }
        int minResult = integers[0];
        for (int i : integers) {
            if (i < minResult) {
                minResult = i;
            }
        }
        return minResult;
    }

    public static String asString(int[] elements) {
        return Arrays.toString(elements).replace("[", "").replace("]", "");
    }

    public static String squareDigits(String s) {
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < s.length(); i++) {
            if (Character.isDigit(s.charAt(i))) {
                int num = Character.getNumericValue(s.charAt(i));
                result.append(num * num);
            } else {
                result.append(s.charAt(i));
            }
        }
        return result.toString();
    }


}
