package collections.streaks;

import java.util.*;

public class Code {

    public static List<List<String>> getStreakList(String input) {
        LinkedList<List<String>> streaks = new LinkedList<>();
        for (char c : input.toCharArray()) {
            String s = String.valueOf(c);
            if (streaks.size() == 0){
                streaks.add(new LinkedList<>(Arrays.asList(s)));
            }
            else if(streaks.getLast().contains(s)){
                streaks.getLast().add(s);

            }
            else {
                streaks.add(new LinkedList<>(Arrays.asList(s)));
            }
        }
        return streaks;
    }
}
