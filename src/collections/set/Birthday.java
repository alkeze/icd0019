package collections.set;

import org.junit.Test;

import java.util.*;


public class Birthday {

    @Test
    public void runCode() {
        List<Integer> experiment = new ArrayList<>();
        for (int i = 0; i <  1000000; i++) {
            experiment.add(findFirst());
        }
        int sum = 0;
        for (Integer integer : experiment) {
            sum += integer;
        }
        System.out.println(sum/ experiment.size());
    }
    private Random r = new Random();
    private int findFirst() {
        Set<Integer> set = new HashSet<>();

        for (int i = 0; i < 365; i++) {
            int randomDayOfYear = r.nextInt(365);
            if(set.contains(randomDayOfYear)){
                return i;
            }else {
                set.add(randomDayOfYear);
            }
        }
        throw new IllegalStateException("programing error");
    }

}
