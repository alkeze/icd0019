package collections.simulator;

import java.util.*;

public class Simulator {

    @SuppressWarnings("PMD.UnusedPrivateField")
    private double iterations;

    public Simulator(double iterations) {
        this.iterations = iterations;
    }

    public Map<HandType, Double> calculateProbabilities() {
        Map<HandType, Double> probabilities = new HashMap<>();

        for (int i = 0; i < iterations; i++) {
            Hand hand = new Hand().getHand();
            switch (hand.getHandType()) {
                case ONE_PAIR:
                    probabilities.put(HandType.ONE_PAIR, probabilities.getOrDefault(HandType.ONE_PAIR, 0D) + 1);
                    break;
                case TWO_PAIRS:
                    probabilities.put(HandType.TWO_PAIRS, probabilities.getOrDefault(HandType.TWO_PAIRS, 0D) + 1);
                    break;
                case TRIPS:
                    probabilities.put(HandType.TRIPS, probabilities.getOrDefault(HandType.TRIPS, 0D) + 1);
                    break;
                case FULL_HOUSE:
                    probabilities.put(HandType.FULL_HOUSE, probabilities.getOrDefault(HandType.FULL_HOUSE, 0D) + 1);
                    break;
                case FLUSH:
                    probabilities.put(HandType.FLUSH, probabilities.getOrDefault(HandType.FLUSH, 0D) + 1);
                    break;
                case STRAIGHT:
                    probabilities.put(HandType.STRAIGHT, probabilities.getOrDefault(HandType.STRAIGHT, 0D) + 1);
                    break;
                case STRAIGHT_FLUSH:
                    probabilities.put(HandType.STRAIGHT_FLUSH, probabilities.getOrDefault(HandType.STRAIGHT_FLUSH, 0D) + 1);
                    break;
                case FOUR_OF_A_KIND:
                    probabilities.put(HandType.FOUR_OF_A_KIND, probabilities.getOrDefault(HandType.FOUR_OF_A_KIND, 0D) + 1);
                default:
                    probabilities.put(HandType.HIGH_CARD, probabilities.getOrDefault(HandType.HIGH_CARD, 0D) +1);

            }
        }
        probabilities.keySet().forEach(k -> probabilities.put(k, probabilities.get(k) * 100 / (iterations)));
        return probabilities;
    }

}
