package collections.simulator;

import java.util.Objects;
import java.util.Random;

public class Card implements Comparable<Card> {

    public enum CardValue { S2, S3, S4, S5, S6, S7, S8, S9, S10, J, Q, K, A;
        public static CardValue getRandomCardValue() {
            Random random = new Random();
            return values()[random.nextInt(values().length)];
        }
    }

    public enum CardSuit { C, D, H, S;

        public static CardSuit getRandomCardSuit() {
            Random random = new Random();
            return values()[random.nextInt(values().length)];
        }
    }
//https://kodejava.org/how-do-i-pick-a-random-value-from-an-enum/

    private final CardValue value;
    private final CardSuit suit;

    public Card(CardValue value, CardSuit suit) {
        this.value = value;
        this.suit = suit;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Card)) {
            return false;
        }

        Card other = (Card) obj;

        return Objects.equals(value, other.value)
                && Objects.equals(suit, other.suit);
    }

    @Override
    public int compareTo(Card other) {
        if(Objects.equals(other.value, value)){
            return 0;
        }
        else if (CardValue.valueOf(value.toString()).ordinal() < CardValue.valueOf(other.value.toString()).ordinal()){
            return -1;
        }
        else {
            return 1;
        }
    }

    public CardValue getValue() {
        return value;
    }

    public CardSuit getSuit() {
        return suit;
    }

    @Override
    public String toString() {
        return String.format("(%s, %s)", value, suit);
    }
}
