package collections.simulator;

import java.util.*;

import static collections.simulator.Card.CardValue.*;

public class Hand implements Iterable<Card> {

    public List<Card> cards = new ArrayList<>();
    private Map<Card.CardValue, Integer> map = new HashMap<>();


    public void addCard(Card card) {
        cards.add(card);
        map.put(card.getValue(), map.getOrDefault(card.getValue(), 0) + 1);
    }

    public Hand getHand() {
        while (cards.size() != 5) {
            Card card = new Card(Card.CardValue.getRandomCardValue(), Card.CardSuit.getRandomCardSuit());
            if (!cards.contains(card)) {
                addCard(card);
            }
        }
        return this;
    }

    @Override
    public String toString() {
        return cards.toString();
    }

    public HandType getHandType() {
        HandType x = simplifyOne();
        if (x != null) {
            return x;
        }
        return simplifyTwo();
    }

    private HandType simplifyTwo() {
        if (isFullHouse()) {
            return HandType.FULL_HOUSE;
        }
        if (isStraightFlush()) {
            return HandType.STRAIGHT_FLUSH;
        }
        if (isFourOfKind()) {
            return HandType.FOUR_OF_A_KIND;
        }
        if (isFlush() && !isStraight()) {
            return HandType.FLUSH;
        }
        return HandType.HIGH_CARD;
    }

    private HandType simplifyOne() {
        if (isOnePair()) {
            return HandType.ONE_PAIR;
        }
        if (isTwoPairs()) {
            return HandType.TWO_PAIRS;
        }
        if (isTrips()) {
            return HandType.TRIPS;
        }
        if (isStraight() && !isFlush()) {
            return HandType.STRAIGHT;
        }
        return null;
    }

    public boolean isOnePair() {
        return Collections.frequency(map.values(), 2) == 1 && !isTrips() && !isFullHouse();
    }

    public boolean isTwoPairs() {
        return Collections.frequency(map.values(), 2) == 2 && !isFullHouse();
    }

    public boolean isTrips() {
        return Collections.frequency(map.values(), 3) == 1 && !isFullHouse();
    }

    public boolean isFullHouse() {
        return map.containsValue(2) && map.containsValue(3);
    }

    public boolean isFourOfKind() {
        return Collections.frequency(map.values(), 4) == 1;
    }

    public boolean isStraight() {
        Collections.sort(cards);

        if (cards.size() != 5) {
            return false;
        }
        if (firsType()) {
            return true;
        }
        for (int i = 0; i < cards.size() - 1; i++) {
            if (cards.get(i).getValue().ordinal() - cards.get(i+1).getValue().ordinal() != -1) {
                return false;
            }
        }
        return true;
    }

    private boolean firsType() {
        return map.containsKey(S2) && map.containsKey(S3) && map.containsKey(S4) && map.containsKey(S5)
                && map.containsKey(A);

    }

    public boolean isFlush() {
        if (cards.size() != 5) {
            return false;
        }
        for (int i = 1; i < cards.size(); i++) {
            if (!(cards.get(i).getSuit().equals(cards.get(i - 1).getSuit()))) {
                return false;
            }
        }
        return true;
    }

    public boolean isStraightFlush() {
        return isFlush() && isStraight();
    }


    public boolean contains(Card card) {
        return cards.contains(card);
    }

    public boolean isEmpty() {
        return cards.isEmpty();
    }

    @Override
    public Iterator<Card> iterator() {
        return cards.iterator();
    }
}
