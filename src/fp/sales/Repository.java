package fp.sales;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.stream.Collectors;

public class Repository {

    private static final String FILE_PATH = "src/fp/sales/sales-data.csv";

    private DateTimeFormatter formatter = DateTimeFormatter
            .ofPattern("dd.MM.yyyy");


    public List<Entry> getEntries(){
        try {
            return readAll().stream()
                    .skip(1)
                    .map(line -> {
                String[] items =  line.split("\t");
                return new Entry(items[2], LocalDate.parse( items[0], formatter)
                        , items[1]
                        , items[3]
                        ,Double.parseDouble( items[5].replace(",", "."))
                        , items[4]);
            }).collect(Collectors.toList());
        }
        catch (Exception e){
            throw new RuntimeException("An error occurred while converting the file " + e);
        }

    }

    private List<String> readAll(){
        try {
            return  Files.readAllLines(Paths.get(FILE_PATH));
        } catch (IOException e) {
           throw new RuntimeException(e);
        }
    }
}
