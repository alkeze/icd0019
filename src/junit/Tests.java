package junit;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.*;

public class Tests {

    @Test
    public void equalityExamples() {
        assertEquals(1, 1);
        assertNotEquals(1, 2);

        Integer x2 = 1;
        Integer y2 = 1;
        assertSame(x2, y2);


        Integer x = 128;
        Integer y = 128;
        assertNotSame(x, y);
        assertEquals(x, y);
    }

    @Test
    public void assertThatAndAssertEqualsExample() {
        assertEquals(1 + 6, 7);

        assertThat(5, is(5));

        assertThat(4, is(equalTo(2 + 2)));

        assertThat("March", is(not("April")));
    }

    @Test
    public void findsSpecialNumbers() {
        assertTrue(Code.isSpecial(0));

        assertTrue(Code.isSpecial(11));

        assertFalse(Code.isSpecial(4));

        assertTrue(Code.isSpecial(36));
    }

    @Test
    public void findsLongestStreak() {
        assertThat(Code.longestStreak(""), is(0));

        assertThat(Code.longestStreak("a"), is(1));

        assertThat(Code.longestStreak("abbcccaaaad"), is(4));

        assertThat(Code.longestStreak("abbcccccaaaad"), is(5));
    }

    @Test
    public void findsModeFromCharactersInString() {

        assertThat(Code.mode(null), is(nullValue()));

        assertThat(Code.mode("aba"), is('a'));

        assertThat(Code.mode("aabbbbaa"), is('a'));

        assertThat(Code.mode("c"), is('c'));
    }

    @Test
    public void removesDuplicates() {
        assertThat(Code.removeDuplicates(arrayOf(1, 1)), is(arrayOf(1)));

        assertThat(Code.removeDuplicates(arrayOf(1, 2, 1, 2)), is(arrayOf(1, 2)));

        assertThat(Code.removeDuplicates(arrayOf(1, 2, 3)), is(arrayOf(1, 2, 3)));
    }

    @Test
    public void sumsIgnoringDuplicates() {
        assertThat(Code.sumIgnoringDuplicates(arrayOf(1, 1)), is(1));

        assertThat(Code.sumIgnoringDuplicates(arrayOf(1, 2, 1, 2)), is(3));

        assertThat(Code.sumIgnoringDuplicates(arrayOf(1, 2, 3)), is(6));
    }

    private int[] arrayOf(int... numbers) {
        return numbers;
    }

}
