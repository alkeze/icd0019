package junit.sales;


public class TopSalesFinder {


    public String[] findItemsSoldOver(int amount, SalesRecord[] records) {
        int uni = findUniq(records);
        int freePosition = 0;
        String[] key = new String[uni];
        int[] value = new int[uni];

        for (SalesRecord s : records) {
            if (!contains(key, s.getProductId())) {
                key[freePosition] = s.getProductId();
                value[freePosition] = s.getProductPrice() * s.getItemsSold();
                freePosition++;
            } else if (contains(key, s.getProductId())) {
                int index = findIndexByName(s.getProductId(), key);
                int curPrice = value[index];
                value[index] = s.getProductPrice() * s.getItemsSold() + curPrice;
            }
        }
        int counter = 0;
        for (String s : key) {
            int index = findIndexByName(s, key);
            if (value[index] > amount) {
                counter++;
            }
        }
        String[] resultArr = new String[counter];
        int pos = 0;
        for (String s : key) {
            int index = findIndexByName(s, key);
            if (value[index] > amount) {
                resultArr[pos++] = s;
            }
        }

        return resultArr;
    }

    private int findUniq(SalesRecord[] records) {
        if (records.length == 0) {
            return 0;
        }

        int res = 1;
        for (int i = 1; i < records.length; i++) {
            int j = 0;
            for (j = 0; j < i; j++) {
                if (records[i].getProductId().equals(records[j].getProductId())) {
                    break;
                }
            }
            if (i == j) {
                res++;
            }
        }
        return res;
    }

    private int findIndexByName(String name, String[] strArr) {
        for (int i = 0; i < strArr.length; i++) {
            if (strArr[i].equals(name)) {
                return i;
            }
        }
        return -1;
    }

    private boolean contains(String[] checkArr, String check) {
        for (String s : checkArr) {
            if (check.equals(s)) {
                return true;
            }
        }
        return false;
    }

}

