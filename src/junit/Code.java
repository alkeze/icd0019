package junit;

public class Code {

    public static boolean isSpecial(Integer number) {
        return number % 11 == 0 || number % 11 < 4;
    }

    public static Integer longestStreak(String input) {
        char[] charters = input.toCharArray();
        if (charters.length == 0) {
            return 0;
        }
        if (charters.length == 1) {
            return 1;
        }
        int strick = 1;
        int strickTemple = 1;
        for (int i = 1; i < charters.length; i++) {
            if (charters[i] != charters[i - 1]) {
                if (strick <= strickTemple) {
                    strick = strickTemple;
                }
                strickTemple = 1;
            } else {
                strickTemple++;
                if (strick <= strickTemple) {
                    strick = strickTemple;
                }

            }
        }
        return strick;
    }

    public static Character mode(String input) {
        if (input == null) {
            return null;
        }
        if (input.equals("")){
            return null;
        }
        char[] chars = input.toCharArray();
        int maxCount = 0;
        char maxValue = 0;

        for (int i = 0; i < input.length(); ++i) {
            int count = 0;
            for (int j = 0; j < input.length(); ++j) {
                if (chars[j] == chars[i]){
                    count++;
                }
            }
            if (count > maxCount) {
                maxCount = count;
                maxValue = chars[i];
            }
        }
        return maxValue;
    }

    public static int getCharacterCount(String input, char c) {
        int count = 0;
        for (char s : input.toCharArray()) {
            if (s == c) {
                count++;
            }
        }
        return count;
    }

    public static int[] removeDuplicates(int[] input) {
        if(input.length == 0){
            return new int[0];
        }

        int res = 1;
        for (int i = 1; i < input.length; i++) {
            int j = 0;
            for (j = 0; j < i; j++) {
                if (input[i] == input[j]) {
                    break;
                }
            }
            if (i == j) {
                res++;
            }
        }
        int[] finalArray = new int[res];
        int position = 0;
        finalArray[position++] = input[0];
        for(int i = 1; i < res; i ++){
            if(input[i] !=input[i - 1]){
                finalArray[position++] = input[i];
            }
        }
        return finalArray;
    }


    public static int sumIgnoringDuplicates(int[] integers) {
        int sumResult = 0;
        if(integers.length == 0){
            return sumResult;
        }
        int[] sum = removeDuplicates(integers);
        for (int value : sum) {
            sumResult += value;
        }
        return sumResult;
    }

}
