package exceptions.pager;

import java.util.ArrayList;
import java.util.List;

public class FilteringPager {

    @SuppressWarnings("PMD.UnusedPrivateField")
    private final SimplePager dataSource;
    @SuppressWarnings("PMD.UnusedPrivateField")
    private final int pageSize;
    int curPage = -1;

    public FilteringPager(SimplePager dataSource, int pageSize) {
        this.dataSource = dataSource;
        this.pageSize = pageSize;
    }

    public List<Integer> getCurrentPage() {
        if (curPage < 0) {
            throw new IllegalStateException("there is no current page");
        } else {
            List<Integer> subList = new ArrayList<>();
            for (int i = curPage * pageSize, j = 0; j < pageSize; i++) {
                for (Integer element : dataSource.getPage(i)) {
                    if (element != null) {
                        subList.add(element);
                        j++;
                    }
                }
            }
            return subList;
        }
    }

    public List<Integer> getNextPage() {
        if (!hasNext()) {
            throw new IllegalStateException("there is no next page");
        } else {
            curPage ++;
            return getCurrentPage();
        }
    }

    public List<Integer> getPreviousPage() {
        if (curPage < 0) {
            throw new IllegalStateException("there is no previous page");
        } else {
            curPage --;
            return getCurrentPage();
        }
    }
    int cap = capacity();

    public boolean hasNext() {
        int pageStart = (curPage + 1) * pageSize;

        return pageStart < cap;
    }

    public int capacity() {
        int couner = 0;
        for (int i = 0; dataSource.hasPage(i); i++) {
            for (Integer element : dataSource.getPage(i)) {
                if (element != null) {
                    couner++;
                }
            }
        }
        return couner;
    }

    @Override
    public String toString() {
        return String.format("page: %s", curPage);
    }
}