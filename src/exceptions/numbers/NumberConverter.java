package exceptions.numbers;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.Properties;

public class NumberConverter {

    private Properties properties = new Properties();
    private FileInputStream inputStream;

    public NumberConverter(String lang) {

        try {
            inputStream = new FileInputStream(String.format("src/exceptions/numbers/numbers_%s.properties", lang));
            InputStreamReader reader = new InputStreamReader(inputStream, StandardCharsets.UTF_8);
            properties.load(reader);
            inputStream.close();
        } catch (FileNotFoundException e) {
            throw new MissingLanguageFileException(lang, e);
        } catch (Exception e) {
            System.out.println(e.toString());
            throw new BrokenLanguageFileException(lang, e);
        }
    }

    public String numberInWords(Integer number) {
        String translatedStr = "";
        char[] numArray = reverseString(number.toString()).toCharArray();
        checkForTranslation();
        try {
            for (int index = 0; index < numArray.length; index++) {

                int digit = Character.getNumericValue(numArray[index]);

                switch (index) {
                    case 0:
                        translatedStr = caseZero(translatedStr, numArray, digit);
                        break;

                    case 1:
                        translatedStr = caseOne(numArray, translatedStr, digit);
                        break;

                    case 2:
                        translatedStr = caseTwo(translatedStr, numArray, digit);
                        break;
                    default:
                        break;
                }
            }
            return translatedStr;
        } catch (Exception e) {
            throw new MissingTranslationException(translatedStr);
        }
    }

    private String caseZero(String translatedStr, char[] numArray, int digit) {
        int teens;
        teens = -1;
        if (numArray.length > 1) {
            teens = Character.getNumericValue(numArray[1]);
        }
        if (!(teens == 1 || teens == 0 && digit == 0)) {
            translatedStr = properties.getProperty(String.valueOf(digit));
        }
        return translatedStr;
    }

    private String caseOne(char[] numArray, String translatedStr, int digit) {
        int digits;
        digits = Character.getNumericValue(numArray[0]);
        int fullNumber = digit * 10 + digits;
        if (digit == 0) {
            return translatedStr;
        } else if (properties.containsKey(String.valueOf(fullNumber))) {
            translatedStr = properties.getProperty(String.valueOf(fullNumber));
        } else if (digit == 1) {
            translatedStr = properties.getProperty(String.valueOf(digits)) + properties.getProperty("teen");
        } else if (digits == 0) {
            translatedStr = properties.getProperty(String.valueOf(digit)) + properties.getProperty("tens-suffix");
        } else {
            if (properties.containsKey(String.valueOf(digit * 10))) {
                translatedStr = properties.getProperty(String.valueOf(digit * 10))
                        + properties.getProperty("tens-after-delimiter") + translatedStr;
            } else {
                translatedStr = properties.getProperty(String.valueOf(digit)) + properties.getProperty("tens-suffix")
                        + properties.getProperty("tens-after-delimiter") + translatedStr;
            }

        }
        return translatedStr;
    }
    
    private String caseTwo(String translatedStr, char[] numArray, int digit) {
        int digits;
        int teens;
        int fullNumber;
        digits = Character.getNumericValue(numArray[0]);
        teens = Character.getNumericValue(numArray[1]);
        fullNumber = digit * 100 + teens * 10 + digits;

        if (properties.containsKey(String.valueOf(fullNumber))) {
            translatedStr = properties.getProperty(String.valueOf(fullNumber));
        } else if (translatedStr.equals("")) {
            translatedStr = properties.getProperty(String.valueOf(digit)) + properties.getProperty("hundreds-before-delimiter")
                    + properties.getProperty("hundred");
        } else {
            translatedStr = properties.getProperty(String.valueOf(digit)) + properties.getProperty("hundreds-before-delimiter")
                    + properties.getProperty("hundred") + properties.getProperty("hundreds-after-delimiter") + translatedStr;
        }
        return translatedStr;
    }

    public String reverseString(String str) {
        StringBuilder reversedString = new StringBuilder();
        for (char chr : str.toCharArray()) {
            reversedString.append(chr);
        }
        return reversedString.reverse().toString();
    }

    public void checkForTranslation() {
        String[] keys = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "teen", "tens-suffix", "tens-after-delimiter", "hundred", "hundreds-before-delimiter", "hundreds-after-delimiter"};
        for (String key : keys) {
            if (!(properties.containsKey(key))) {
                throw new MissingTranslationException(key);
            }
        }
    }
}

