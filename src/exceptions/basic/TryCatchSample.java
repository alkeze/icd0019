package exceptions.basic;

public class TryCatchSample {
    public String readDataFrom(Resource resource) {
        String file;
        try{
            resource.open();
            file = resource.read();
        }catch (Exception e){
            return "someDefaultValue";
        }
        finally {
            resource.close();
        }
        return file;
    }
}
