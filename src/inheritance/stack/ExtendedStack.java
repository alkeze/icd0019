package inheritance.stack;

import java.util.Stack;

public class ExtendedStack extends Stack<Integer> {
    @Override
    public Integer push(Integer item) {
        System.out.println(String.format("Pushing %s into stack.", item));
        return super.push(item);
    }

    @Override
    public synchronized Integer pop() {
        System.out.println("Removing the last element from the stack.");
        return super.pop();
    }

    public void pushAll(Integer... numbers){
        for (Integer number : numbers) {
            push(number);

        }
    }
}
