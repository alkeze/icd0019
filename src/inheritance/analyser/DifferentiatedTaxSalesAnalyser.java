package inheritance.analyser;

import java.util.List;

public class DifferentiatedTaxSalesAnalyser extends AbstractAnalyser {

    private final Double taxRateHight;
    private final Double taxRateLow;
    private final List<SalesRecord> records;

    public DifferentiatedTaxSalesAnalyser(List<SalesRecord> rec) {
        super(rec);
        taxRateHight = 0.2;
        taxRateLow = 0.1;
        records = super.getSalesRecords();
    }

    @Override
    public Double getTotalSalesByProductId(String id) {

        Double sales = 0D;

        for (SalesRecord record : records) {
            if (id.equals(record.getProductId()))
                if (record.hasReducedRate()) {
                    sales += record.getItemsSold() * record.getProductPrice() / (1 + taxRateLow);
                }
                else {
                    sales += record.getItemsSold() * record.getProductPrice() / (1 + taxRateHight);
                }
        }

        return sales;
    }
}
