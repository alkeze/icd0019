package inheritance.analyser;

import java.util.ArrayList;
import java.util.List;

public abstract class AbstractAnalyser {
    private final List<SalesRecord> salesRecords;
    private final String[] ids;

    public AbstractAnalyser(List<SalesRecord> rec) {
        salesRecords = rec;
        ids = getIds();
    }

    protected List<SalesRecord> getSalesRecords(){
        return salesRecords;
    }

//    protected String[] getIDS(){
//        return is;
//    }


    public Double getTotalSales() {

        Double totalSales = 0D;

        for (String id : ids) {
            totalSales += getTotalSalesByProductId(id);
        }

        return totalSales;
    }

    public abstract Double getTotalSalesByProductId(String id);

    public String getIdOfMostPopularItem() {

        String saleId = null;
        Integer value = null;

        for (String id : ids) {
            if (saleId == null || value < allInfoById(id)[0]){
                saleId = id;
                value = allInfoById(id)[0];
            }
        }

        return saleId;
    }

    public String getIdOfItemWithLargestTotalSales() {

        String saleId = null;
        Double value = null;

        for (String id : ids) {

            if (saleId == null || value < getTotalSalesByProductId(id)){
                saleId = id;
                value = getTotalSalesByProductId(id);
            }
        }

        return saleId;
    }

    protected String[] getIds(){

        ArrayList<String> listOfId = new ArrayList<>();
        String id;

        for (SalesRecord record : salesRecords) {
            id = record.getProductId();
            if (!(listOfId.contains(id))){
                listOfId.add(id);
            }
        }

        return listOfId.toArray(new String[listOfId.size()]);
    }

    protected Integer[] allInfoById(String id){
        Integer[] info = {0, 0};
        for (SalesRecord record : salesRecords) {
            if (id.equals(record.getProductId())){
                info[0] += record.getItemsSold();
                info[1] = record.getProductPrice();
            }
        }
        return info;
    }
}
