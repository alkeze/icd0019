package inheritance.analyser;

import java.util.List;

public class FlatTaxSalesAnalyser extends AbstractAnalyser {

    private final Double taxRate;

    public FlatTaxSalesAnalyser(List<SalesRecord> rec) {
        super(rec);
        taxRate = 0.2;
    }

    @Override
    public Double getTotalSalesByProductId(String id) {

        Integer[] info = allInfoById(id);
        return info[0] * info[1] / (1 + taxRate);

    }

}
